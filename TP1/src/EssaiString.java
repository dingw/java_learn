import javax.swing.*;

public class EssaiString {
    public static void main(String[] args) {
        String s1 = "RT";
        String s2 = "rt".toUpperCase();
        System.out.println("s1 = " + s1);
        System.out.println("s2 = " + s2);

        boolean test1 = (s1 == "RT");
        System.out.println("test1 = " + test1);
        boolean test2 = (s2 == "RT");
        System.out.println("test2 = " + test2);
        boolean test3 = s1.equals("RT");
        System.out.println("test3 = " + test3);
        boolean test4 = s2.equals("RT");
        System.out.println("test4 = " + test4);


        String departement = JOptionPane.showInputDialog("Quel est ton département à l'IUT ?");
        if (departement.equals("RT")) {
            JOptionPane.showMessageDialog(null, "Tu as fait le bon choix !");
        } else {
            JOptionPane.showMessageDialog(null, "Mais qu'es tu allé faire en " +
                    departement +
                    " ? Tu aurais bien mieux fait d'aller en RT !");
        }
    }
}
