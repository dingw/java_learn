public class EssaiPoint {
    public static void main(String[] args) {
        Point p1, p2, p3;

        p1 = new Point(1,2);
        p2 = new Point(3,4);
        p3 = p1;

        if(p1 == p2){
            System.out.println("p1 == p2");
        }
        if(p1 == p3){
            System.out.println("p1 == p3");
        }

        p1.x = 1;
        p1.y = 2;
        p2.x = 3;
        p2.y = 4;
        p3.x = 5;

        System.out.println("p1 =  " + p1);
        System.out.println("p2 = " + p2);
        System.out.println("p3 = " + p3);

        p2 = null;
        System.out.println("p2 = " + p2);
        System.out.println("Fin");

        Point p4 = new Point(2, 6);
        Point p5 = new Point(2, 6);
        System.out.println("p4 = " + p4);
        System.out.println("p5 = " + p5);

        if(p4.equals(p5)) {
            System.out.println("Les points p4 et p5 ont les mêmes coordonnées");
        } else {
            System.out.println("Les points p4 et p5 sont différents");
        }

        System.out.println("p1.dist2() = " + p1.dist2());
        System.out.println("p1.dist2(p4) = " + p1.dist2(p4));

        System.out.println("Point.dist2(p1, p4) = " + Point.dist2(p1, p4));
        System.out.println("Point.dist2(new Point(3,2), new Point(1,5)) = " +
                            Point.dist2(new Point(3,2), new Point(1,5)));

    }
}
