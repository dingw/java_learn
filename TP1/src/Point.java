import static java.lang.Math.hypot;

public class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int dist2(){
        return  this.x * this.x + this.y * this.y;
    }

    public int dist2(Point p){
        int deltaX = this.x - p.x;
        int deltaY = this.y - p.y;
        return  deltaX * deltaX + deltaY * deltaY;
    }

    public static int dist2(Point p1, Point p2){
        return p1.dist2(p2);
    }

    public static double dist(Point p1, Point p2){
        return hypot(p1.x - p2.x, p1.y - p2.y);
    }

    @Override
    public String toString() {
        return super.toString() + "{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }
}
