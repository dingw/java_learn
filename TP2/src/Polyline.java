import java.util.Arrays;

public class Polyline {
    Point[] sommets;

    public Polyline(Point[] sommets) {
        this.sommets = sommets;
    }

    public Polyline(int nbSommets) {
        sommets = new Point[nbSommets];
    }

    public void ajoute(Point p){
        for (int i=0; i<this.sommets.length; i++)
            if (this.sommets[i] == null) {
                this.sommets[i] = p;
                break;
            }
    }

    public void ajoute(int x,int y){
        this.ajoute(new Point(x, y));
    }

    public double longueur(){
        double longueur = 0;
        for (int i=0; i< sommets.length-1; i++){
            if (sommets[i] != null && sommets[i+1] != null)
                longueur += Point.dist(sommets[i], sommets[i+1]);
        }
        return longueur;

    }

    @Override
    public String toString() {
        String str = "{";
        for (int i=0; i<sommets.length; i++){
            if (sommets[i] != null) {
                if (i != 0)
                    str += ", ";
                str += sommets[i].toString();
            }
        }
        return str + "}";
    }
}



