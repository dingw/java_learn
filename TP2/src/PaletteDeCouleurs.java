import java.awt.*;

public class PaletteDeCouleurs {
    public static void main(String[] args) {
        String[] colorNames = {"noir", "blanc", "rouge", "vert", "bleu", "cyan",
                "magenta", "jaune", "gris foncé", "gris", "gris clair", "orange"};
        int[] rgbValues = {0x000000, 0xffffff, 0xff0000, 0x00ff00, 0x0000ff, 0x00ffff,
                0xff00ff, 0xffff00, 0x444444, 0x888888, 0xcccccc, 0xed7f10};

        Color palette[];
        palette = new Color[rgbValues.length];
        for (int i=rgbValues.length-1; i>=0; i--){
            palette[i]= new Color(rgbValues[i]);
            System.out.println(String.format("%2d. %10s : %s", i+1, colorNames[i], palette[i]));
        }



    }



}
