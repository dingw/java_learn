
public class EssaiIPv4 {
    public static void main(String[] args) {
        IPv4 ip1 = new IPv4();
        System.out.println("ip1 = " + ip1);
        System.out.println("ip1.getMask() = " + ip1.getMask());
        System.out.println("ip1.getBinaryIp() = " + ip1.getBinaryIp());
        System.out.println("ip1.getBinaryMask() = " + ip1.getBinaryMask());
        IPv4 ip2 = new IPv4(192, 168, 42, 15, 26);
        System.out.println("ip2 = " + ip2);
        System.out.println("ip2.getMask() = " + ip2.getMask());
        System.out.println("ip2.getBinaryIp() = " + ip2.getBinaryIp());
        System.out.println("ip2.getBinaryMask() = " + ip2.getBinaryMask());
        IPv4 ip3 = new IPv4("172.31.230.98/13");
        System.out.println("ip3 = " + ip3);
        System.out.println("ip3.getMask() = " + ip3.getMask());
        System.out.println("ip3.getBinaryIp() = " + ip3.getBinaryIp());
        System.out.println("ip3.getBinaryMask() = " + ip3.getBinaryMask());
    }
}