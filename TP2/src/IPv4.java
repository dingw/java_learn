import java.util.Arrays;

public class IPv4 {

    int[] ipAdr;
    int msk;

    public IPv4() {
        this.ipAdr = new int[] {0, 0, 0, 0};
        this.msk = 0;
    }

    public IPv4(String ipStr) {

        this.ipAdr = new int[4];
        String[] ipAdrStr = ipStr.split("/")[0].split("\\.");
        String mskStr = ipStr.split("/")[1];

        try {
            for (int i=0; i<ipAdrStr.length; i++) {
                this.ipAdr[i] = Integer.parseInt(ipAdrStr[i]);
            }
            this.msk = Integer.parseInt(mskStr);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input: " + e.getMessage());
        }
    }

    public IPv4(int adr1, int adr2, int adr3, int adr4, int msk) {
        this.ipAdr = new int[] {adr1, adr2, adr3, adr4};
        this.msk = msk;
    }

    public String getBinaryMask() {
        String binaryMskStr = "";
        for (int i=0; i<this.ipAdr.length*8; i++){
            if (i % 8 == 0 && i != 0) {
                binaryMskStr += '.';
            }
            if (i<this.msk) {
                binaryMskStr += '1';
            }
            else {
                binaryMskStr += '0';
            }
        }
        return binaryMskStr;
    }

    public String getMask() {
        String mskStr = "";
        String[] binaryMskStr = this.getBinaryMask().split("\\.");
        for (int i=0; i<binaryMskStr.length; i++){
            if (i != 0)
                mskStr += ".";
            mskStr += Integer.toString(Integer.parseInt(binaryMskStr[i], 2));
        }
        return mskStr;
    }

    public String getBinaryIp() {
        String binaryIpAdrStr = "";
        for (int i=0; i<this.ipAdr.length; i++){
            if (i!=0)
                binaryIpAdrStr += ".";
            String binaryStr = Integer.toBinaryString(ipAdr[i]);
            binaryStr = "0".repeat(8-binaryStr.length()) + binaryStr; // padding
            binaryIpAdrStr += binaryStr;
        }
        return binaryIpAdrStr;
    }

    @Override
    public String toString() {
        String str = "";
        for (int i=0; i<this.ipAdr.length; i++){
            if (i != 0)
                str += ".";
            str += ipAdr[i];
        }
        str += "/" + this.msk;
        return str;
    }
}
