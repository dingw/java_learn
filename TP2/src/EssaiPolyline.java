import java.util.Scanner;

public class EssaiPolyline {
    public static Polyline saisiePoint(){
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter(",|\n|\r\n");

        System.out.print("Number of points = ");
        int nbSommets = sc.nextInt();
        Polyline pl = new Polyline(nbSommets);

        for (int i=0; i<nbSommets; i++){
            System.out.printf("Point no. %d = ", i);
            int x = sc.nextInt();
            int y = sc.nextInt();
            pl.ajoute(x, y);
        }

        return pl;
    }
    public static void main(String[] args) {
        Point[] pts = {new Point(1,1), new Point(3,1), new Point(3,5)};
        Polyline pl1 = new Polyline(pts);
        Polyline pl2 = new Polyline(4);
        pl2.ajoute(new Point(2,1));
        pl2.ajoute(3,5);
        Polyline pl3 = new Polyline(0);

        System.out.println("pl1 = " + pl1);
        System.out.println("pl2 = " + pl2);
        System.out.println("pl3 = " + pl3);

        System.out.println("pl1.longueur() = " + pl1.longueur());
        System.out.println("pl2.longueur() = " + pl2.longueur());
        System.out.println("pl3.longueur() = " + pl3.longueur());

        Polyline pl4 = saisiePoint();
        System.out.println("pl4 = " + pl4);
        System.out.println("pl4.longueur() = " + pl4.longueur());

    }

}
