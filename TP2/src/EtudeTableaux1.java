import java.util.Arrays;

public class EtudeTableaux1 {

    /**
     * Point d'entrée du programme
     * @param args paramètres de la ligne de commande
     */
    public static void main(String[] args) {

        System.out.println("* tableau d'int");
        int[] tab1;
        tab1 = new int[5];
        for (int i = 0; i < tab1.length; i++) {
            tab1[i] = i*i;
        }
        System.out.println("tab1 = " + Arrays.toString(tab1));
        System.out.println("tab1 = " + tab1.toString());

        System.out.println("* tableau de double");
        double[] tab2 = { 1.1, 2.2, 3.3, 4.4, 5.5 };
        System.out.println("tab2 = " + Arrays.toString(tab2));
        System.out.println("tab2 = " + tab2.toString());

        System.out.println("* tableau de char");
        char[] tab3 = { 'R', '&', 'T'};
        System.out.println("tab3 = " + Arrays.toString(tab3));
        System.out.println("tab3 = " + tab3.toString());
    }
}