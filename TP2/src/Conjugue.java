import java.awt.*;

public class Conjugue {
    public static void main(String[] args) {
        String[] subject = {"je", "tu", "il", "nous", "vous", "ils"};
        String[] terminations = {"e", "es", "e", "ons", "ez", "ent"};

        if (args.length != 1) {
            System.out.println("Vous devez passer un verbe du 1er groupe en paramètre");
            System.exit(-1);
        }
        String verbe = args[0];
        if (!verbe.endsWith("er")) {
            System.out.println("Vous devez passer un verbe du 1er groupe en paramètre");
            System.exit(-1);
        }

        System.out.println("verbe = " + verbe);
        String radical = verbe.substring(0, verbe.length() - 2);
        for (int i = 0; i < terminations.length; i++) {
            System.out.println(subject[i] + " " + radical + terminations[i]);
        }


    }
}
