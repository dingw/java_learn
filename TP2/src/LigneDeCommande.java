import java.util.Arrays;

public class LigneDeCommande {
    public static void main(String[] args) {
        System.out.println("args.length = " + args.length);
        for (int i=0; i < args.length; i++){
            System.out.println(String.format("args[%d] = %s", i, args[i]));
        }
        System.out.println("Arrays.toString(args) = " + Arrays.toString(args));
    }
}
