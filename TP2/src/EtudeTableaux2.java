import java.awt.*;
import java.util.Arrays;

public class EtudeTableaux2 {
    /**
     * Point d'entrée du programme
     * @param args paramètres de la ligne de commande
     */
    public static void main(String[] args) {
        Color[] palette1 = {Color.MAGENTA, Color.CYAN, Color.YELLOW};
        System.out.println("palette1 = " + Arrays.toString(palette1));

        Color[] palette2;
        palette2 = new Color[3];
        palette2[0] = new Color(255, 0, 0);
        palette2[1] = new Color(0, 255, 0);
        palette2[2] = new Color(0, 0, 255);
        System.out.println("palette2 = " + Arrays.toString(palette2));

        Color presqueNoir = new Color(3);
        System.out.println("presqueNoir = " + presqueNoir);
    }
}